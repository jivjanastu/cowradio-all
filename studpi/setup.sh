#!/bin/bash
tilde=/home/pi/cowradio
############################################

if [[ $HOSTNAME != *"cowrec"* ]]; then

   read -p "### Enter new name for this pi 'cowstudpi-" id
   ctr=1
   while [ -z "$id"]
   do
      if ((ctr>2)); then
         echo "### No name entered. Exiting installation ..."
         exit 1
      fi
      read -p "### Enter new name for this pi 'cowstudpi-" id
      ctr=$(($ctr+1))
   done
   name=cowstudpi-$id
   sed -i "s/$HOSTNAME/$name/g" /etc/hosts
   echo $name > /etc/hostname
   echo "### The new name for this pi is now '$name' "
   echo "### You can use 'ssh pi@$name.local' to login into this pi"
   #/etc/init.d/hostname.sh
   #hostname $name
   printf "### --------------------------------- ###\n\n"
   echo "### Changing login password ..."
   echo "### Please enter simple one that you can remember"
   passwd
   printf "### --------------------------------- ###\n\n"
fi

############################################

echo "### Checking internet connectivity ..."
if ping www.gitlab.com -c 1 -w 1 > /dev/null; then
   echo "### Connected to internet!!!"
else
   echo "### Not connected to internet!!!"
   echo "### Please connect to internet and retry"
   echo "### Exiting installation..."
   exit 1
fi
printf "### --------------------------------- ###\n\n"

############################################

echo "### Updating package lists ..."
apt-get update
printf "### --------------------------------- ###\n\n"

############################################

echo "### Downloading cowradio files ..."
wget -O - https://gitlab.com/jivjanastu/cowradio/-/archive/master/cowradio-master.tar.gz | tar xzf -
mv cowradio-master cowradio
printf "### --------------------------------- ###\n\n"

############################################

echo "### Installing Python GPIO Libraries ..."
apt-get install python3-rpi.gpio python3-gpiozero
printf "### --------------------------------- ###\n\n"

echo "### Installing OGG Encoder ..."
apt-get install vorbis-tools
printf "### --------------------------------- ###\n\n"

echo "### Installing OMX Player ..."
apt-get install omxplayer
printf "### --------------------------------- ###\n\n"

echo "### Installing NGINX and PHP ..."
apt-get install nginx php-fpm
cp $tilde/recpi/docs/default /etc/nginx/sites-available/default
chown -R www-data:pi /var/www/html/
chmod -R 770 /var/www/html/
cp -r $tilde/recpi/html/* /var/www/html/
/etc/init.d/nginx restart
printf "### --------------------------------- ###\n\n"

echo "### Installing Hotspot Features ..."
apt-get install hostapd dnsmasq
systemctl stop hostapd
systemctl stop dnsmasq

cp $tilde/recpi/docs/hostapd.conf /etc/hostapd/hostapd.conf
sed -r -i "s/ssid=[A-Za-z]+/ssid=$idRadio/g" /etc/hostapd/hostapd.conf
systemctl unmask hostapd
systemctl enable hostapd
sed -r -i "s/.*address=.*/address=\/$id.radio\/10.1.33.54/g" $tilde/recpi/ap_mode/dnsmasq.conf

#cp $tilde/recpi/docs/sysctl.conf /etc/sysctl.conf
cp $tilde/recpi/docs/rc.local /etc/rc.local
printf "### --------------------------------- ###\n\n"

############################################

read -p "Going to reboot now. Press enter to continue..."
reboot
printf "### --------------------------------- ###\n\n"

############################################
: <<'END'
ctr=1
while ! [ -x "$(command -v git)" ]
do
   echo "Installing git. Attempt no. #$ctr"
   apt-get install git
   ctr=$(($ctr+1))
   if ((ctr>3)); then
      echo "Installing git failed. Please check internet connection and try again"
      exit 1
   fi
done
echo "Git installed!!!"

echo "Cloning repo from gitlab"
git clone https://gitlab.com/jivjanastu/cowradio.git
if [-d ./cowradio]; then
   echo "Repo cloned"
fi
END
#############################################

