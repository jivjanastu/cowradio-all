# COW-Radio
Community Owned/Operated Wireless (COW) Radio

A DIY radio station that provides a platform for a community to truly ***express*** themselves.

The technological backbone comprises of Raspberry-Pis that can be classified as
1. Recorder Pi
2. Studio Pi

## Recorder Pi (RecPi)
This can come in various forms and names. Its primary functions are
1. Recording audio
2. Playback recorded audio
3. Host a webpage to playback recorderd and published audio
4. Sync content with StudPi - 
    * Push recorded audio to StudPi
    * Receive new playlist from StudPi
5. Have a hotspot over which the webpage can be accessed
6. Optional/Additional - Have a loudspeaker for playback

Based on the forms, we can broadly classify RecPis into two categories
1. Portable Recorder Pi (PRecPi) -
    * Primary role is to serve as recorder for field work
2. Community Recorder Pi (CommRecPi) - 
    * Primary role is to serve as radio playback device for the community
    * Usually has a loudspeaker for playback

## Studio Pi (StudPi)
It serves the following functions:
1. Receiving and backing up of files from RecPis
2. Editing audio using Audacity
3. Creating a playlist of audio files to publish on the network
4. Sync the plalist with RecPis when connected

## WorkFlow
