# Rec Pi

## Install

On a fresh [Raspbian Lite](https://www.raspberrypi.org/downloads/raspbian/) install run:
```bash
bash <(curl -sSL https://gitlab.com/jivjanastu/cowradio/-/raw/master/recpi/setup.sh)
```
