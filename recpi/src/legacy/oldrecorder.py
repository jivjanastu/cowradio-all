from time import sleep
from keypad import KeyPad
from dualled import DualLED
from datetime import datetime

import os
import subprocess


# *** Global Variables *** #
# setting folder paths
projectpath    = os.path.split(os.path.realpath(__file__))[0]
audioguidepath = projectpath + "/data/audio-alert"
recordingpath  = projectpath + "/data/recordings"
recordingpublishpath = "/var/www/html/recordings"

# gpio pins used 
play_led    = LED(22)
record_led  = LED(26)

play_btn   = Button (3, hold_time = 2)
record_btn = Button (19, hold_time = 2)
plus_btn   = Button (21, hold_time = 2)
minus_btn  = Button (18, hold_time = 2)

# button flags
play_btn_held    = 0
record_btn_held  = 0
plus_btn_held    = 0
minus_btn_held   = 0

play_btn_press    = 0
record_btn_press  = 0
plus_btn_press    = 0
minus_btn_press   = 0

# vlc playlist player
instance = vlc.Instance("-A alsa --alsa-audio-device plughw:1,0")
listPlayer = instance.media_list_player_new()

#lplayer = instance.media_list_player_new()


def play_audio(audiofile):
    """ macro to playing audio instructions - to keep the code simple """
    subprocess.call("cvlc -A alsa --alsa-audio-device plughw:1 --no-loop --play-and-exit " +
                    audioguidepath+"/"+audiofile, shell=True)

def load_playlist():
    #Load the media file
    subprocess.call("ls -t " + recordingpath + "/*.ogg > " + projectpath+"/test.m3u",
                shell=True)

    with open(projectpath+'/test.m3u','r') as f:
        dialog = f.readlines()


        # Remove newline, carriage return
        dialog = [w.replace('\n', '').replace('\r','') for w
                  in dialog]

        mediaList = instance.media_list_new()
        for music in dialog:
            mediaList.add_media(instance.media_new('file:///'+music))

    return(mediaList)
'''
def vol_plus():
    global plus_btn_held
    if plus_btn_held==1:
        plus_btn_held = 0
        return
    print("Plus Button pressed ")
'''

def play_next():
    global plus_btn_held
    global play_btn_press
    
    plus_btn_held = 1
    print("Plus Button Held")
    
    listPlayer.next()
    if not play_btn_press: listPlayer.pause()

'''
def vol_minus():
    global minus_btn_held
    if minus_btn_held==1:
        minus_btn_held = 0
        return
    print("Minus Button pressed ")

'''
def play_prev():
    global minus_btn_held
    global play_btn_press

    minus_btn_held = 1    
    print("Minus Button Pressed")
    
    listPlayer.previous()
    if not play_btn_press: listPlayer.pause()


def play_pause_audio():
    global play_btn_held
    global play_btn_press
    if play_btn_held==1:
        play_btn_held = 0
        return

    if play_btn_press:
        listPlayer.pause()
        play_led.off()
        play_btn_press = 0
        sleep(0.5)
        return

    listPlayer.play()
    play_led.on()
    play_btn_press = 1

def initialize_player(audiofile):
    "initialize  a vlc player which plays locally and saves to an mp3file"
    inst = vlc.Instance('-A alsa')
    p = inst.media_player_new()
    cmd0 = "alsa://plughw:1,0"
    cmd1 = "sout=#transcode{acodec=ogg,ab=320}:file{dst=%s}" %audiofile

    med=inst.media_new(cmd0,cmd1)
    med.get_mrl()
    p.set_media(med)
    return p

def record_audio():
    global record_btn_held
    global sink
    global listPlayer

    record_btn_held = 1
    record_led.on()
    record_btn.wait_for_release()

    # play record begin alert message
    #play_audio(sink,"beep.wav")
    sleep(0.5)
    listPlayer.release()

    # records with 48000 quality
    tp = subprocess.Popen("arecord -D plughw:1,0 "+projectpath+"/recorded_audio.wav -f dat ", shell=True)
    
    # scan for button press to stop recording
    record_btn.wait_for_press()
    record_btn_held = 0
    record_led.off()
    subprocess.call(["pkill","-9","arecord"])
    tp.kill()
    sleep(0.5)
    
    # converting recorded audio to mp3 and rename with date and time of recording
    record_led.blink(0.05,0.05)
    p = subprocess.Popen("oggenc "+projectpath+"/recorded_audio.wav "+"-b 320 -o "+recordingpath+"/recorded@"+datetime.now().strftime('%d%b%Y_%H:%M')+".ogg ",
                    shell=True)
    p.wait()

    mediaList = load_playlist()
    #for i in mediaList:
    #subprocess.call("sshpass -p 'raspberrystudio' rsync -r "+recordingpath+"/"+" pi@studiopi.local:/home/pi/Documents/",shell=True)
    listPlayer = instance.media_list_player_new()
    listPlayer.set_media_list(mediaList)

    record_led.off()


if __name__ == "__main__":
    #play_audio("lappiready.wav")

    #plus_btn.when_held = play_next
    plus_btn.when_released = play_next

    #minus_btn.when_held = play_prev
    minus_btn.when_released = play_prev

    #play_btn.when_held = play_next
    play_btn.when_released = play_pause_audio

    #record_btn.when_released = play_next
    record_btn.when_held = record_audio

    mediaList = load_playlist()

    listPlayer.set_media_list(mediaList)
    player = instance.media_player_new()
    player.audio_set_volume(100)

    while True:

        """
        if play_btn.is_pressed:
            play_led.on()
        elif record_btn.is_pressed:
            record_led.on()
        else:
            play_led.off()
            record_led.off()
        """
        sleep(100)