import vlc
import subprocess
import time

instance = vlc.Instance()

#Create a MediaPlayer with the default instance
player = instance.media_player_new()

#Load the media file
subprocess.call("ls -t /home/pi/Documents/cowmesh-radio/cowmesh-radio/data/recordings/*.mp3 > test.m3u",
                shell=True)


with open('test.m3u','r') as f:
    dialog = f.readlines()
    

    # Remove newline, carriage return
    dialog = [w.replace('\n', '').replace('\r','') for w 
                  in dialog]
    
    mediaList = instance.media_list_new()
    for music in dialog:
            mediaList.add_media(instance.media_new('file:///'+music))
    listPlayer = instance.media_list_player_new()
    listPlayer.set_media_list(mediaList)
    

listPlayer.play()
time.sleep(10)
listPlayer.next()
time.sleep(10)
