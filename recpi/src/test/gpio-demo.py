from gpiozero import LED, Button
from time import sleep
import subprocess

play_led    = LED(22)
record_led  = LED(26)

play_btn   = Button (3, hold_time = 2)



record_btn = Button (19, hold_time = 2)

# button flags
play_btn_held    = 0
record_btn_held  = 0
plus_btn_held    = 0
minus_btn_held   = 0

play_btn_press    = 0
record_btn_press  = 0
plus_btn_press    = 0
minus_btn_press   = 0


print("Hello World")

def record_audio():
    record_btn_held = 1
    record_led.on()
    record_btn.wait_for_release()

    sleep(0.5)
    
    # records with 48000 quality
    tp = subprocess.Popen("arecord -D plughw:1,0 recorded_audio.wav -f dat ", shell=True)
    
    # scan for button press to stop recording
    record_btn.wait_for_press()
    record_btn_held = 0
    record_led.off()
    tp.kill()
    sleep(0.5)
gtp=0
def play_pause_audio():
    global play_btn_held
    global play_btn_press
    global gtp
    if play_btn_held==1:
        play_btn_held = 0
        return
    play_led.on()
    play_btn.wait_for_release()

    #if play_btn_press:
     #   gtp.kill()
      #  play_led.off()
       # play_btn_press = 0
        #sleep(0.5)
        #return

    gtp = subprocess.Popen("aplay -D plughw:1,0 recorded_audio.wav", shell=True)

    play_btn_press = 1
    play_btn.wait_for_press()
    gtp.kill()
    play_led.off()


record_btn.when_held = record_audio
play_btn.when_held = play_pause_audio

while True:
 	
	#play_led.on()
	#record_led.off()
	sleep(5)
	#play_led.off()
	#record_led.on()
	#sleep(0.5)
