import os
import subprocess
from time import sleep
from keypad import KeyPad
from dualled import DualLED
from datetime import datetime


# *** Global Variables *** #
# setting folder paths
projectpath    = os.path.split(os.path.realpath(__file__))[0]
audioguidepath = projectpath + "/data/audio-alert"
recordingpath  = projectpath + "/data/recordings"
recordingpublishpath = "/var/www/html/audios"

playpause = False
recording = False
star_held_release = False
aplay_state = False
chrome_state = False
debug_mode = False


obj = None
led = None
rprocess = None
cp = None

def play_instruction(filename):
    subprocess.call("aplay -D plughw:1,0 "+audioguidepath+"/"+filename,
                    shell=True)
    
def play_recording():
    global aplay_state
    subprocess.call("omxplayer -o alsa:hw:1,0 "+projectpath+"/recorded_audio.wav &",
                    shell=True)

def star_release():
    global rprocess
    global recording
    global playpause
    global star_held_release
    global debug_mode

    if recording:
        rprocess.kill()
        subprocess.call(["pkill","-9","arecord"])
        #subprocess.call(["pkill","-9","ffmpeg"])
        sleep(0.1)
        
        led.rev_blink("fast")
        p = subprocess.Popen("oggenc "+projectpath+"/recorded_audio.wav "+" -q 10 -o "+
                            recordingpublishpath+"/recorded@"+datetime.now().strftime('%d%b%Y_%H:%M')+".ogg ",
                             shell=True)
        p.wait()

        #subprocess.call(["sudo","cp",projectpath+"/recorded_audio.ogg",
        #                 recordingpublishpath+"/recorded@"+datetime.now().strftime('%d%b%Y_%H:%M')+".ogg"])
        
        if not debug_mode: led.fwd_on()
        else: led.rev_on()
        
        recording = False

    elif playpause:
        subprocess.call(["pkill","-9","omxplayer"])
        
        if not debug_mode: led.fwd_on()
        else: led.rev_on()
        
        playpause = False
        return
    else:
        led.fwd_blink("slow")
        play_recording()
        playpause = True
        return

    
def star_held():
    global rprocess
    global recording
    global star_held_release
    subprocess.call(["pkill","-9","omxplayer"])
    rprocess = subprocess.Popen("arecord -D plughw:1,0 --format S16_LE --rate 48000 "+projectpath+"/recorded_audio.wav &", shell=True)
    #rprocess = subprocess.Popen("ffmpeg -f alsa -ac 2 -ar 44100 -i hw:1,0 -y "+projectpath+"/recorded_audio.ogg &", shell=True)
     
    led.rev_blink("slow")

    recording = True
    star_held_release = True
    print(recording)
    

def hash_release():
    global chrome_state
    global cp
    global debug_mode
        
    if chrome_state:
        print("Send pkill to browser")
        subprocess.call(["pkill","-9","chromium-browse"])
        cp.kill()
        if not debug_mode: led.fwd_on()
        else: led.rev_on()
        chrome_state = False
        
    else:
        chrome_state = True       
        led.blink("slow")
        subprocess.call(["pkill","-9","omxplayer"])
        cp = subprocess.Popen("chromium-browser --app=http://localhost &", shell=True)
        sleep(5)

def hash_held():
    global debug_mode
    led.blink("fast")

    if not debug_mode:
        debug_mode = True
        temp = subprocess.Popen("/bin/bash "+projectpath+"/../../client_mode/client_mode.sh", shell=True)
        temp.wait()
        led.rev_on()
    else:
        debug_mode = False
        temp = subprocess.Popen("/bin/bash "+projectpath+"/../../ap_mode/ap_mode.sh", shell=True)
        temp.wait()
        led.fwd_on()

def one_release():
	print("Switch ONE released")

def two_held():
    if debug_mode:
        led.blink("slow")
        temp = subprocess.Popen("/bin/bash "+projectpath+"/sync.sh",shell=True)
        temp.wait()
        led.rev_on()

def three_release():
        print("Switch THREE released") 

def four_release():
        print("Switch FOUR released") 



if __name__ == "__main__":
    obj = KeyPad( [13,6,26,19],[4], timeout=2,
                  when_released=[[star_release,None,three_release,four_release]],
		  when_held=[[star_held,two_held,None,hash_held]])
    #             when_released=[[hash_release,None,star_release]], 
    #             when_held=[[hash_held,None,star_held]],
    #             )
    led = DualLED(21,20)
    led.fwd_on()
    obj.run()
