#!/bin/bash
tilde=/home/pi/cowradio
############################################

if [[ $HOSTNAME != *"cowrecpi"* ]]; then

   read -p "### Enter new name for this pi 'cowrecpi-" id
   ctr=1
   while [ -z "$id" ]
   do
      if ((ctr>2)); then
         echo "### No name entered. Exiting installation ..."
         exit 1
      fi
      read -p "### Enter new name for this pi 'cowrecpi-" id
      ctr=$(($ctr+1))
   done
   name=cowrecpi-$id
   sudo sed -i "s/$HOSTNAME/$name/g" /etc/hosts
   echo $name | sudo tee /etc/hostname
   echo "### The new name for this pi is now '$name' "
   echo "### You can use 'ssh pi@$name.local' to login into this pi"
   #/etc/init.d/hostname.sh
   #hostname $name
   printf "### --------------------------------- ###\n\n"
   echo "### Changing login password ..."
   echo "### Please enter simple one that you can remember"
   passwd
   printf "### --------------------------------- ###\n\n"
else
   id=${HOSTNAME#"cowrecpi-"}
fi

############################################

echo "### Checking internet connectivity ..."
if ping www.gitlab.com -c 1 -w 1 > /dev/null; then
   echo "### Connected to internet!!!"
else
   echo "### Not connected to internet!!!"
   echo "### Please connect to internet and retry"
   echo "### Exiting installation..."
   exit 1
fi
printf "### --------------------------------- ###\n\n"

############################################

echo "### Updating package lists ..."
sudo apt-get -y update
printf "### --------------------------------- ###\n\n"

############################################

#echo "### Installing git ..."
ctr=1
while ! [ -x "$(command -v git)" ]
do
   echo "### Installing git. Attempt no. #$ctr"
   sudo apt-get install git
   ctr=$(($ctr+1))
   if ((ctr>3)); then
      echo "Installing git failed. Please check internet connection and try again"
      exit 1
   fi
done
echo ".. Git installed!!! ###"

printf "### --------------------------------- ###\n\n"

############################################

echo "### Downloading cowradio files ..."
#wget -O - https://gitlab.com/jivjanastu/cowradio/-/archive/master/cowradio-master.tar.gz | tar xzf -
#mv cowradio-master cowradio
git clone https://gitlab.com/jivjanastu/cowradio.git
if [ -d ./cowradio ]; then
   printf "... Repo cloned!!! ###"
   cd cowradio
   git config user.email "you@example.com"
   git config user.name "Your Name"
   cd ..
fi
printf "### --------------------------------- ###\n\n"

############################################

echo "### Installing Python GPIO Libraries ..."
sudo apt-get -y install python3-rpi.gpio python3-gpiozero
printf "### --------------------------------- ###\n\n"

echo "### Installing OGG Encoder ..."
sudo apt-get -y install vorbis-tools
printf "### --------------------------------- ###\n\n"

echo "### Installing OMX Player ..."
sudo apt-get -y install omxplayer
printf "### --------------------------------- ###\n\n"

echo "### Installing NGINX and PHP ..."
sudo apt-get -y install nginx php-fpm
sudo cp $tilde/docs/default /etc/nginx/sites-available/default
sudo chown -R www-data:pi /var/www/html/
sudo chmod -R 770 /var/www/html/
sudo cp -r $tilde/recpi/html/* /var/www/html/
sudo /etc/init.d/nginx restart
printf "### --------------------------------- ###\n\n"

echo "### Installing sshpass ..."
sudo apt-get -y install sshpass
printf "### --------------------------------- ###\n\n"

############################################

echo "### Installing Hotspot Features ..."
sudo apt-get -y install hostapd dnsmasq
sudo systemctl stop hostapd
sudo systemctl stop dnsmasq

sudo cp $tilde/docs/hostapd.conf /etc/hostapd/hostapd.conf
sudo sed -r -i "s/.*ssid=.*/ssid=$id Radio/g" /etc/hostapd/hostapd.conf
sudo systemctl unmask hostapd
sudo systemctl enable hostapd
sudo sed -r -i "s/.*address=.*/address=\/$id.radio\/10.1.33.54/g" $tilde/ap_mode/dnsmasq.conf

#sudo cp $tilde/docs/sysctl.conf /etc/sysctl.conf

printf "### --------------------------------- ###\n\n"


############################################

echo "### Updating rc.local for applications to load on startup ..."
sudo cp $tilde/docs/rc.local /etc/rc.local
printf "### --------------------------------- ###\n\n"


############################################

read -p "Going to reboot now. Press enter to continue..."
sudo reboot
printf "### --------------------------------- ###\n\n"

############################################
